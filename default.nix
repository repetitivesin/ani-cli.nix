{ name
, lib
, stdenv
, fetchFromGitHub
, makeWrapper
, curl
, openssl
, mpv
, aria
, ffmpeg
, ani-cli-git
}:

stdenv.mkDerivation rec {
  inherit name;
  src = ani-cli-git;
  buildInputs = [ openssl curl mpv aria ffmpeg ];
  nativeBuildInputs = [ makeWrapper ];
  installPhase = ''
    mkdir $out/bin -p
    cp $src/ani-cli $out/bin
    wrapProgram $out/bin/ani-cli \
      --prefix PATH : ${lib.makeBinPath buildInputs}
  '';
}
