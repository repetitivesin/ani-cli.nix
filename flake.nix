{
  description = "ani-cli package";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-21.11";
    flake-utils.url = "github:numtide/flake-utils";
    ani-cli-git.url = "github:pystardust/ani-cli";
    ani-cli-git.flake = false;
  };

  outputs = { self, nixpkgs, flake-utils, ani-cli-git }:
    {
      overlays.default = final: prev: {
        ani-cli = self.packages.${prev.system}.default;
      };
    } //
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        name = "ani-cli";
      in
      {
        packages = rec {
          ani-cli = pkgs.callPackage ./default.nix { inherit name ani-cli-git; };
          default = ani-cli;
          hello = pkgs.hello;
        };
      });
}
